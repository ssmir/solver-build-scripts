FROM python:3.8.13-bullseye as buildoptimizer

WORKDIR /builddir

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow

RUN apt-get update \
    && apt-get install --no-install-recommends -y ca-certificates apt-utils \
    && update-ca-certificates

COPY install-base-packages-debian.sh .
RUN bash install-base-packages-debian.sh

ARG IPOPT_VERSION=3.13.3
ARG HSL_PATH=./coinhsl-2014.01.10.zip
ARG WITH_MUMPS=1
ARG NCPU=1

COPY ${HSL_PATH} coinhsl.zip
COPY build-ipopt.sh .
RUN bash build-ipopt.sh ${IPOPT_VERSION} /usr/local ${WITH_MUMPS} /builddir/coinhsl.zip

ARG CBC_VERSION=2.10.7
COPY build-cbc.sh .
RUN bash build-cbc.sh ${CBC_VERSION} /usr/local

ARG BLISS_VERSION=0.73.3
COPY build-bliss.sh .
RUN bash build-bliss.sh ${BLISS_VERSION} /usr/local

ARG SCIP_VERSION=8.0.1
ARG SCIP_COMMIT=
COPY patches ./patches
COPY build-scip-cmake.sh .
RUN bash build-scip-cmake.sh ${SCIP_VERSION} /usr/local 1 ${SCIP_COMMIT}

ARG PYSCIPOPT_VERSION=4.2.0
ENV SCIPOPTDIR=/usr/local
RUN pip install cython && \
    wget https://github.com/scipopt/PySCIPOpt/archive/refs/tags/v${PYSCIPOPT_VERSION}.tar.gz && \
    tar xzf v${PYSCIPOPT_VERSION}.tar.gz && \
    cd PySCIPOpt-${PYSCIPOPT_VERSION} && \
    python -m pip install .

ARG HIGHS_VERSION=1.3.0
COPY build-highs.sh .
RUN bash build-highs.sh ${HIGHS_VERSION} /usr/local

COPY requirements.txt .
RUN pip install -r requirements.txt

FROM python:3.8.13-bullseye AS packageoptimizer

WORKDIR /usr/local

COPY --from=buildoptimizer /usr/local .
COPY --from=buildoptimizer /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages

WORKDIR /workdir

RUN apt-get update && apt-get install --no-install-recommends -y \
    libgfortran5 libopenblas-base libmetis5 libgomp1 libtbb2 libcliquer1 \
    libboost-serialization1.74.0 libgsl25 libboost-program-options1.74.0 \
    libncurses6 libgmpxx4ldbl libboost-iostreams1.74.0 \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
