CONTAINER=solvers
REGISTRY=distcomp
PUSH_TAG=$(shell git rev-parse --abbrev-ref HEAD)

#override tag if set in parameter
ifdef TAG
  PUSH_TAG=$(TAG)
endif

#set "latest" tag from master branch
ifeq ($(PUSH_TAG), master)
  PUSH_TAG=latest
endif

build:
	echo Building $(REGISTRY)/$(CONTAINER)\:$(PUSH_TAG)
	docker build --build-arg NCPU=4 --rm -t $(REGISTRY)/$(CONTAINER)\:$(PUSH_TAG) .

build-7.0.3:
	echo Building $(REGISTRY)/$(CONTAINER)\:7.0.3
	docker build --build-arg NCPU=4 --build-arg SCIP_VERSION=7.0.3 --build-arg PYSCIPOPT_VERSION=3.3.0 --rm -t $(REGISTRY)/$(CONTAINER)\:7.0.3 .

build-test:
	echo Testing build-solvers.sh
	docker build -f Dockerfile.test --rm .

push:
	docker push $(REGISTRY)/$(CONTAINER)\:$(PUSH_TAG)
