#!/bin/bash
NCPU=$(lscpu | grep '^CPU(s):' | awk '{ print $2 }')
BONMIN_VER="$1"

PREFIX=${PREFIX:-/usr/local}

CURRENT="$PREFIX/bin/bonmin"
if [ -e "$CURRENT" ]; then
    VER=$($CURRENT -v | awk '/Bonmin/ {gsub(/\./, ""); print $2}')
    mv -f $CURRENT $CURRENT$VER
fi

if ! [ -e Bonmin-$BONMIN_VER ]; then
    curl -L http://www.coin-or.org/download/source/Bonmin/Bonmin-$BONMIN_VER.tgz | tar xz
fi
cd Bonmin-*
cd ThirdParty/ASL
./get.ASL
cd ../..
rm -rf build
mkdir -p build
cd build

if [ -a /usr/lib/libblas.so ]; then
    BLAS="-lblas -lpthread -lrt"
    LAPACK="-llapack $BLAS"
else
    BLAS="-L$PREFIX/lib -lopenblas -lpthread -lrt"
    LAPACK=""
fi

export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig
../configure --prefix=$PREFIX CXXFLAGS=-fopenmp FCFLAGS=-fopenmp \
    LDFLAGS="-Wl,-rpath,$PREFIX/lib" \
    --with-blas="$BLAS" \
    --with-lapack="$LAPACK" --without-hslold \
    --with-ipopt-lib="$(pkg-config ipopt --libs)" \
    --with-ipopt-incdir="$(pkg-config ipopt --cflags)" \
    --with-cbc-lib="$(pkg-config cbc --libs)" \
    --with-cbc-incdir="$(pkg-config cbc --cflags)" \
    --with-hsl-lib="$(pkg-config coinhsl --libs)" \
    --with-hsl-incdir="$(pkg-config coinhsl --cflags)" \
    --with-mumps-lib="$(pkg-config coinmumps --libs)" \
    --with-mumps-incdir="$(pkg-config coinmumps --cflags)" \
    --with-metis-lib="$(pkg-config coinmetis --libs)" \
    --with-metis-incdir="$(pkg-config coinmetis --cflags)"
make -j $NCPU
make install
cd
rm -rf Bonmin-*
