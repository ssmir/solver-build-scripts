#!/bin/bash
set -e

if [ "$(uname -s)" = "Darwin" ]; then
    OSX=1
    NCPU=$(sysctl hw.physicalcpu | awk '{print $2}')
else
    NCPU=$(lscpu | grep '^CPU(s):' | awk '{ print $2 }')
fi

VER="$1"
PREFIX="$2"

if [ -e $PREFIX/lib/libopenblas.a ] || [ -e $PREFIX/lib/libopenblas.so ]; then
    BLAS="--with-blas=-L$PREFIX/lib -lopenblas -lpthread -lrt"
    LAPACK="--with-lapack="
else
    BLAS="--with-blas=-lblas -lpthread -lrt"
    LAPACK="--with-lapack=-llapack $BLAS"
fi

BLAS="--with-blas=-lopenblas -lpthread -lrt"
LAPACK="--with-lapack=-lopenblas -lpthread -lrt"

if [ $OSX ]; then
    BLAS=""
    LAPACK=""
fi

if [ ! -e coinbrew ]; then
    curl https://raw.githubusercontent.com/coin-or/coinbrew/master/coinbrew > coinbrew
fi
set +e
bash coinbrew fetch --main-proj=Cbc --main-proj-version=releases/$VER --no-prompt
set -e

bash coinbrew build --main-proj=Cbc --main-proj-version=releases/$VER \
     "$BLAS" "$LAPACK" --enable-cbc-parallel --prefix=$PREFIX --no-prompt --parallel-jobs=$NCPU

bash coinbrew install --main-proj=Cbc --main-proj-version=releases/$VER --no-prompt

echo ""
echo "CBC installed successfully in $PREFIX"
echo ""
