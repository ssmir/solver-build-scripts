#!/bin/bash
set -e

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

VER="$1"
PREFIX="$2"

if [ ! -e ddbnb-tmp ]; then
    git clone https://github.com/distcomp/ddbnb.git ddbnb-tmp
fi
cd ddbnb-tmp/c_src
export PATH=$PREFIX/bin:$PATH

SCIP_SRC=../../scipoptsuite-${VER}/scip
CPPFLAGS="-DNO_CONFIG_HEADER -I$SCIP_SRC/interfaces/ampl/solvers -I$SCIP_SRC/src -I$SCIP_SRC/interfaces/ampl/src"
SCIP_PORT_SRC="$SCIP_SRC/interfaces/ampl/src/reader_nl.c ErlPortInterface.cc event_all.cc scip_port.cc"
g++ -o scip_port $CPPFLAGS $SCIP_PORT_SRC -L$SCIP_SRC/lib/static -L$HOME/built/lib \
    -lscip -llpispx2 -lnlpi.cppad.ipopt -ltpinone -lsoplex.linux.x86_64.gnu.opt \
    -lbliss.linux.x86_64.gnu \
    -lipopt -lz  -lreadline -lpthread $SCIP_SRC/interfaces/ampl/solvers/amplsolver.a -ldl
cd ..
# if [ ! -e ampl/build/lib/libasl.a ]; then
#     ./bootstrap.sh
# fi
# make -C c_src CBC_HOME=$PREFIX SCIP_SRC=../../scipoptsuite-$VER/scip SCIP_HOME=$PREFIX SCIP_LIBS="-L$PREFIX/src/scip/lib/static -L$PREFIX/lib -lscip -lobjscip -llpispx2 -lnlpi.cppad.ipopt -ltpinone -lsoplex.linux.x86_64.gnu.opt -lipopt -lz -lgmp -lreadline -lpthread"
cp -f c_src/scip_port $PREFIX/bin
cd ..
rm -rf ddbnb-tmp
