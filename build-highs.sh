#!/bin/bash
set -e
VER="$1"
PREFIX="$2"
if [ $# -ne 2 ]; then
    echo "Usage: $0 <HiGHS version> <prefix>"
    exit
fi

PREFIX=${PREFIX:-/usr/local}
NCPU=$(lscpu | grep '^CPU(s):' | awk '{ print $2 }')

curl -L https://github.com/ERGO-Code/HiGHS/archive/refs/tags/v${VER}.tar.gz | tar xz
cd HiGHS-${VER}
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$PREFIX
make -j $NCPU
make install
cd ../src/interfaces/highspy
pip install ./
cd ../../../..
rm -rf HiGHS-${VER}
