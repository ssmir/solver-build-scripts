#!/bin/bash
set -e
VER="$1"
PREFIX="$2"
WITH_MUMPS="$3"
HSL_PATH="$4"
if [ $# -ne 4 ]; then
    echo "Usage: $0 <ipopt version> <prefix> <with_mumps> <hsl_path>"
    exit
fi

PREFIX=${PREFIX:-/usr/local}
IPOPT_CURRENT=$($PREFIX/bin/ipopt -v | awk '{print $2}')

if [ ! -e $PREFIX/bin/ipopt -o "$IPOPT_CURRENT" != "$VER" ]; then
    mkdir -p ipopt-build
    cd ipopt-build
    wget https://raw.githubusercontent.com/coin-or/coinbrew/v1.0/coinbrew
    chmod +x coinbrew
    ./coinbrew fetch Ipopt@$VER --no-prompt
    unzip -n $HSL_PATH
    mv coinhsl-* ThirdParty/HSL/coinhsl
    MUMPS=""
    if [ "$WITH_MUMPS" == 0 ]; then
        MUMPS="--without-mumps"
    fi
    ./coinbrew build Ipopt --prefix=$PREFIX --test --no-prompt --parallel-jobs ${NCPU:-1} --verbosity=3 $MUMPS LDFLAGS="-Wl,-rpath,$PREFIX/lib"
    cd ..
    rm -rf ipopt-build
else
    echo "Skipping ipopt" $VER "build: already installed in $PREFIX/bin"
fi
