#!/bin/bash
set -e
OPENBLAS_VER="$1"
PREFIX="$2"
if [ ! -d OpenBLAS-$OPENBLAS_VER ]; then
    curl -L https://github.com/xianyi/OpenBLAS/archive/v$OPENBLAS_VER.tar.gz | tar xz
fi
cd OpenBLAS-$OPENBLAS_VER
LDFLAGS=-Wl,-rpath,$PREFIX/lib make USE_OPENMP=1
make PREFIX=$PREFIX install
cd ..
rm -rf OpenBLAS-$OPENBLAS_VER
