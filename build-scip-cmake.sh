#!/bin/bash
set -e
VER="$1"
PREFIX="$2"
WITH_PARASCIP="$3"
SCIP_COMMIT="$4"
if [ $# -ne 3 -a $# -ne 4 ]; then
    echo "Usage: $0 <scip version> <prefix> <with_parascip> [scip_commit]"
    exit 1
fi

if [ "$WITH_PARASCIP" == 1 ]; then
    WITH_PARASCIP=true
else
    WITH_PARASCIP=false
fi

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PREFIX=${PREFIX:-/usr/local}
NCPU=${NCPU:-$(lscpu | grep '^CPU(s):' | awk '{ print $2 }')}
SCIP_FLAGS=("USRLDFLAGS=-Wl,-rpath,$PREFIX/lib -lgomp" IPOPT=true ZIMPL=false OPT=opt GMP=false SYM=bliss PARASCIP=$WITH_PARASCIP)
if [ ! -e $PREFIX/bin/scip ]; then
    if ! [ -e scipoptsuite-$VER ]; then
        curl -L -k https://scip.zib.de/download/release/scipoptsuite-$VER.tgz | tar xz
        cd scipoptsuite-$VER

        if [ "$SCIP_COMMIT" != "" ]; then
            rm -rf scip
            git clone https://github.com/scipopt/scip.git
            cd scip
            git checkout $SCIP_COMMIT
        else
            cd scip
        fi

        mkdir -p lib/static
        ln -sfn $PREFIX lib/static/ipopt.linux.x86_64.gnu.opt

        mkdir -p lib/include
        ln -sfn $PREFIX/include/bliss lib/include/bliss
        ln -sfn $PREFIX/lib/libbliss.a lib/static/libbliss.linux.x86_64.gnu.a

        case "$VER" in
            "3.1.0")
                patch -p 1 < $SCRIPT_DIR/patches/scipampldualsol2.patch
                ;;
            "5.0.1")
                patch -p 1 < $SCRIPT_DIR/patches/mem-limit.patch
                ;;
            "6.0.0")
                cd ..
                tar xzfv $SCRIPT_DIR/patches/scipoptsuite-6.0.0_patch01.tgz
                tar xzfv $SCRIPT_DIR/patches/parascip-6.0.0-patch.tgz
                cd scip
                ;;
            "6.0.1")
                cd ..
                tar xzfv $SCRIPT_DIR/patches/scipoptsuite-6.0.1_patch01.tgz
                cd scip
                ;;
            "6.0.2")
                set +e
                patch -p 1 < $SCRIPT_DIR/patches/scipoptsuite-6.0.2_nlreader.patch
                cd ../ug
                patch -p 1 < $SCRIPT_DIR/patches/ug-088.patch
                set -e
                cd ../scip
                ;;
            "7.0.0")
                set +e
                cd ../ug
                patch -p 1 < $SCRIPT_DIR/patches/scipParaInstance-7.0.0.patch
                set -e
                cd ../scip
                ;;
            "8.0.0")
                set +e
                cd ../scip
                patch -p 1 < $SCRIPT_DIR/patches/scip-8.0.0-interactive.patch
                set -e
                cd ../scip
                ;;
            "8.0.1")
                set +e
                cd ../scip
                patch -p 1 < $SCRIPT_DIR/patches/scip-8.0.0-interactive.patch
                set -e
                cd ../scip
                ;;
        esac
        if [[ "${VER:0:1}" -lt 8 ]]; then
            cd interfaces/ampl
            ./get.ASL
            cd solvers
            sh configurehere
            make
            cd ../../..
        fi
        cd ../..
    fi

    cd scipoptsuite-$VER
    mkdir -p build
    cd build

    PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig cmake .. -DCMAKE_INSTALL_PREFIX=$PREFIX -DAMPL=YES -DTHREADSAFE=on -DTPI=omp # -DCMAKE_BUILD_TYPE=Debug

    make -j $NCPU
    make install
    cp -vf bin/papilo $PREFIX/bin
    cd ../..
else
    echo "Skipping scip build: already installed in $PREFIX/bin"
fi

if [ ! -e $PREFIX/bin/scipampl ]; then
  if [ "${VER:0:1}" -lt 8 ]; then
    cd scipoptsuite-$VER/build
    make scipampl
    cp bin/interfaces/ampl/scipampl $PREFIX/bin/
    cd ../..
  fi
else
    echo "Skipping scipampl build: already installed in $PREFIX/bin"
fi

if [ "$WITH_PARASCIP" == "true" -a ! -e $PREFIX/bin/parascip ]; then
  if [ "${VER:0:1}" -lt 8 -a "$(which mpicxx)" ]; then
    cd scipoptsuite-$VER
    set +e
    patch -N -p 1 < ug/scip.patch
    set -e
    make -j $NCPU ug "${SCIP_FLAGS[@]}" COMM=mpi
    cp -v ug/bin/parascip $PREFIX/bin/
    cd ..
  fi
else
    echo "Skipping parascip build: already installed in $PREFIX/bin"
fi

if [ "$WITH_PARASCIP" == "true" -a ! -e $PREFIX/bin/fscip ]; then
  if [ "${VER:0:1}" -lt 8 ]; then
    cd scipoptsuite-$VER
    set +e
    patch -N -p 1 < ug/scip.patch
    set -e
    make -j $NCPU ug "${SCIP_FLAGS[@]}" COMM=pth
    cp -v ug/bin/fscip $PREFIX/bin/
    cd ..
  fi
else
    echo "Skipping fscip build: already installed in $PREFIX/bin"
fi

#rm -fr scipoptsuite-$VER
