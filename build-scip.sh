#!/bin/bash
set -e
VER="$1"
PREFIX="$2"
WITH_PARASCIP="$3"
if [ $# -ne 3 ]; then
    echo "Usage: $0 <scip version> <prefix> <with_parascip>"
    exit
fi

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PREFIX=${PREFIX:-/usr/local}
NCPU=$(lscpu | grep '^CPU(s):' | awk '{ print $2 }')
SCIP_FLAGS=("USRLDFLAGS=-Wl,-rpath,$PREFIX/lib -lgomp" IPOPT=true ZIMPL=false OPT=opt GMP=false SYM=bliss PARASCIP=true)

if [ ! -e $PREFIX/bin/scip ]; then
    if ! [ -e scipoptsuite-$VER ]; then
        curl -L -k https://scip.zib.de/download/release/scipoptsuite-$VER.tgz | tar xz
    fi
    cd scipoptsuite-$VER

    set +e
    tar xzf scip-$VER.tgz
    set -e
    if [ -e scip-$VER ]; then
        mkdir -p scip-$VER/lib
        ln -sf $PREFIX scip-$VER/lib/ipopt.linux.x86_64.gnu.opt
        mkdir -p scip-$VER/lib/static
        ln -sf $PREFIX scip-$VER/lib/static/ipopt.linux.x86_64.gnu.opt
        SCIP_DIR=scip-$VER
    else
        mkdir -p scip/lib/static
        ln -sfn $PREFIX scip/lib/static/ipopt.linux.x86_64.gnu.opt
        SCIP_DIR=scip
    fi

    mkdir -p $SCIP_DIR/lib/include
    if [ ! -e $SCIP_DIR/lib/include/bliss ]; then
        cd $SCIP_DIR/lib/include
        curl -L http://www.tcs.hut.fi/Software/bliss/bliss-0.73.zip > bliss-0.73.zip
        unzip bliss-0.73.zip
        mv bliss-0.73 bliss
        cd bliss
        set +e
        patch -p1  < $SCRIPT_DIR/patches/bliss-0.73.patch
        set -e
        make
        cd ../../../..
    fi
    ln -sfn $PWD/$SCIP_DIR/lib/include/bliss/libbliss.a \
       $SCIP_DIR/lib/static/libbliss.linux.x86_64.gnu.a

    cd $SCIP_DIR
    case "$VER" in
        "3.1.0")
            patch -p 1 < $SCRIPT_DIR/patches/scipampldualsol2.patch
            ;;
        "5.0.1")
            patch -p 1 < $SCRIPT_DIR/patches/mem-limit.patch
            ;;
        "6.0.0")
            cd ..
            tar xzfv $SCRIPT_DIR/patches/scipoptsuite-6.0.0_patch01.tgz
            tar xzfv $SCRIPT_DIR/patches/parascip-6.0.0-patch.tgz
            cd scip
            ;;
        "6.0.1")
            cd ..
            tar xzfv $SCRIPT_DIR/patches/scipoptsuite-6.0.1_patch01.tgz
            cd scip
            ;;
        "6.0.2")
            set +e
            patch -p 1 < $SCRIPT_DIR/patches/scipoptsuite-6.0.2_nlreader.patch
            cd ../ug
            patch -p 1 < $SCRIPT_DIR/patches/ug-088.patch
            set -e
            cd ../scip
            ;;
        "7.0.0")
            set +e
            cd ../ug
            patch -p 1 < $SCRIPT_DIR/patches/scipParaInstance-7.0.0.patch
            set -e
            cd ../scip
            ;;
    esac
    cd ..

    #echo $PREFIX | make -j $NCPU scipoptlib "${SCIP_FLAGS[@]}"
    yes '' | make -j $NCPU scipoptlib "${SCIP_FLAGS[@]}"
    cp lib/*.a $PREFIX/lib

    # echo "Copying SCIP source code to $PREFIX/src/scip-$VER"
    # mkdir -p $PREFIX/src
    # #cp -rL $SCIP_DIR $PREFIX/src/scip-$VER
    # rsync -RrL --exclude $SCIP_DIR/lib/static/ipopt.linux.x86_64.gnu.opt $SCIP_DIR $PREFIX/src/
    # mv $PREFIX/src/$SCIP_DIR $PREFIX/src/scip-$VER
    # chmod -R o+r $PREFIX/src/scip-$VER
    # ln -sf $PREFIX/src/scip-$VER $PREFIX/src/scip

    cd $SCIP_DIR
    make -j $NCPU install "${SCIP_FLAGS[@]}" INSTALLDIR=$PREFIX
    cd ../..
else
    echo "Skipping scip build: already installed in $PREFIX/bin"
fi

if [ ! -e $PREFIX/bin/scipampl ]; then
    if [ -e scipoptsuite-$VER/scip-$VER ]; then
        SCIP_DIR=scip-$VER
    else
        SCIP_DIR=scip
    fi
    cd scipoptsuite-$VER/$SCIP_DIR/interfaces/ampl

    ./get.ASL
    cd solvers
    sh configurehere
    make
    cd ..
    make "${SCIP_FLAGS[@]}"
    cp bin/scipampl $PREFIX/bin/
    cd ../../../..
else
    echo "Skipping scipampl build: already installed in $PREFIX/bin"
fi

if [ "$WITH_PARASCIP" == 1 -a ! -e $PREFIX/bin/parascip ]; then
    cd scipoptsuite-$VER
    set +e
    patch -N -p 1 < ug/scip.patch
    set -e
    make -j $NCPU ug "${SCIP_FLAGS[@]}" COMM=mpi
    cp -v ug/bin/parascip $PREFIX/bin/
    cd ..
else
    echo "Skipping parascip build: already installed in $PREFIX/bin"
fi

if [ "$WITH_PARASCIP" == 1 -a ! -e $PREFIX/bin/fscip ]; then
    cd scipoptsuite-$VER
    set +e
    patch -N -p 1 < ug/scip.patch
    set -e
    make -j $NCPU ug "${SCIP_FLAGS[@]}" COMM=pth
    cp -v ug/bin/fscip $PREFIX/bin/
    cd ..
else
    echo "Skipping fscip build: already installed in $PREFIX/bin"
fi

#rm -fr scipoptsuite-$VER
