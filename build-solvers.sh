#!/bin/bash

set -e

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

. $SCRIPT_DIR/config.sh

CBC_CURRENT=$(echo | $PREFIX/bin/cbc | grep Version | awk '{print $2}')
IPOPT_CURRENT=$($PREFIX/bin/ipopt -v | awk '{print $2}')
SCIP_CURRENT=$($PREFIX/bin/scip -v | grep version | awk '{print $3}')
OPENBLAS_CURRENT=$(ls -1 $PREFIX/lib/ | grep 'openblas.*r.*so' | sed 's/^.*-r\(.*\).so/\1/')

WITH_MUMPS=1
if [ "$WITH_PARASCIP" == 1 ]; then
    WITH_MUMPS=0
fi

mkdir -p $PREFIX

if [ "$OPENBLAS_VER" != "" -a "$OPENBLAS_CURRENT" != "$OPENBLAS_VER" ]; then
    echo "Installing OpenBLAS $OPENBLAS_VER"
    bash $SCRIPT_DIR/build-openblas.sh $OPENBLAS_VER $PREFIX
fi

if [ "$IPOPT_VER" != "" -a "$IPOPT_CURRENT" != "$IPOPT_VER" ]; then
    echo "Installing Ipopt $IPOPT_VER"
    bash $SCRIPT_DIR/build-ipopt.sh $IPOPT_VER $PREFIX $WITH_MUMPS $HSL_PATH
fi

if [ "$CBC_VER" != "" -a "$CBC_CURRENT" != "$CBC_VER" ]; then
    echo "Installing CBC $CBC_VER"
    bash $SCRIPT_DIR/build-cbc.sh $CBC_VER $PREFIX
fi


bash $SCRIPT_DIR/build-bliss.sh $BLISS_VER $PREFIX

if [ "$SCIP_VER" != "" -a "$SCIP_CURRENT" != "$SCIP_VER" ]; then
    echo "Installing SCIP $SCIP_VER"
    bash $SCRIPT_DIR/move-scip.sh $PREFIX
    bash $SCRIPT_DIR/build-scip-cmake.sh $SCIP_VER $PREFIX $WITH_PARASCIP $SCIP_COMMIT
fi

if [ "$SCIP_VER" != "" -a "$WITH_DDBNB" == "1" ]; then
    echo "Installing DDBNB for SCIP"
    bash $SCRIPT_DIR/build-ddbnb.sh $SCIP_VER $PREFIX
fi

#if ! [ -e $PREFIX/bin/bonmin ]; then
#    bash bonmin.sh $BONMIN_VER
#fi

echo "Done!"
