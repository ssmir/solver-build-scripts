# Installation path
export PREFIX=/usr/local
#export PREFIX=$HOME/built

# Build FiberSCIP and ParaSCIP along with SCIP solver
export WITH_PARASCIP=1

# Build DDBNB drivers for CBC and SCIP solvers
export WITH_DDBNB=0

# Build OpenBLAS
#OPENBLAS_VER=0.3.3

# Build Ipopt solver
IPOPT_VER=3.13.3

# Build CBC solver
#CBC_VER=2.10.2

# Build Bliss library with patches required for SCIP compatibility
BLISS_VER=0.73.3

# Build SCIPOptSuite
SCIP_VER=8.0.1

# If not empty, builds specified SCIP commit from https://github.com/scipopt/scip repository
SCIP_COMMIT=

# Build Bonmin solver
#BONMIN_VER=1.8.6

# Global path to the HSL for IPOPT distribution archive (https://www.hsl.rl.ac.uk/ipopt/)
HSL_PATH=/builddir/coinhsl-2014.01.10.zip
