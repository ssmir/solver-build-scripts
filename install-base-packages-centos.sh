#!/bin/bash
set -e

yum -y install epel-release
yum -y install git python-pip gcc-c++ python-devel time screen patch zip unzip \
    nano emacs-nox autoconf automake libtool jq md5deep wget gcc-gfortran gmp-devel \
    zlib-devel readline-devel bison flex cmake git subversion ncurses-devel openblas-devel nss
