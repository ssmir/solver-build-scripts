#!/bin/bash
set -e
apt-get install --no-install-recommends -y \
    pkg-config build-essential gfortran patch wget perl unzip \
    libgmp-dev zlib1g-dev libreadline-dev bison flex libncurses5-dev cmake git \
    libpthread-stubs0-dev libtbb-dev libboost-serialization-dev libboost-program-options-dev \
    libmpfrc++-dev libgsl-dev libcliquer-dev libmetis-dev file libboost-iostreams-dev \
    libopenblas-dev curl
