#!/bin/bash
set -e
# if ! [ -a /etc/apt/sources.list.d/erlang-solutions.list ]; then
#   wget http://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
#   dpkg -i erlang-solutions_1.0_all.deb
#   sed -i 's/binaries.erlang-solutions.com/packages.erlang-solutions.com/' \
#       /etc/apt/sources.list.d/erlang-solutions.list
#   rm erlang-solutions_1.0_all.deb
# fi

apt-get update
apt-get install -y pkg-config build-essential gfortran patch wget perl unzip nano \
    libgmp-dev zlib1g-dev libreadline-dev bison flex libncurses5-dev cmake git \
    libpthread-stubs0-dev libtbb-dev libboost-all-dev libmpfrc++-dev libgsl-dev libbliss-dev libcliquer-dev libmetis-dev \
    subversion python-simplejson python-argparse python3 #erlang-base-hipe erlang-nox 
