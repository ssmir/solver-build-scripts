#!/bin/bash
set -e

PREFIX="$1"
if [ $# -ne 1 ]; then
    echo "Usage: $0 <prefix>"
    exit
fi

CURRENT="$PREFIX/bin/scip"
if [ -e "$CURRENT" ]; then
    VER=$($CURRENT -c quit | awk '/SCIP version/ {gsub(/\./, ""); print $3}')
    if [ "$VER" == "" ]; then
        VER="_"
    fi
    mv -fv $CURRENT $CURRENT$VER
    mv -fv $PREFIX/bin/scipampl $PREFIX/bin/scipampl$VER
    if [ -e $PREFIX/bin/parascip ]; then
        mv -fv $PREFIX/bin/parascip $PREFIX/bin/parascip$VER
    fi
    if [ -e $PREFIX/bin/fscip ]; then
        mv -fv $PREFIX/bin/fscip $PREFIX/bin/fscip$VER
    fi
    if [ -e $PREFIX/bin/papilo ]; then
        mv -fv $PREFIX/bin/papilo $PREFIX/bin/papilo$VER
    fi
    rm -f $PREFIX/src/scip
fi
